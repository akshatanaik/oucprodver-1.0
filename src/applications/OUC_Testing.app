<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>OUC Development and Evaluation</description>
    <label>OUC Testing</label>
    <logo>SharedDocuments/OUC_Logo1.jpg</logo>
    <tab>standard-Case</tab>
    <tab>Agent_Console</tab>
    <tab>ServiceRequestTypeConfig</tab>
    <tab>FlexNoteQuestionConfig</tab>
    <tab>ServiceRequestType__c</tab>
    <tab>ServiceRequestTypeFlexNote__c</tab>
    <tab>FlexNoteQuestion__c</tab>
    <tab>FlexNote__c</tab>
    <tab>OUC_Activity__c</tab>
    <tab>Jurisdiction__c</tab>
    <tab>ServiceRequestType_Jurisdiction__c</tab>
    <tab>WorkflowRule__c</tab>
    <tab>Service_Request_History__c</tab>
    <tab>Flex_Note_History__c</tab>
    <tab>OUC_Activity_History__c</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>Decode_Object__c</tab>
    <tab>Activity_Type__c</tab>
    <tab>Activity_Outcomes__c</tab>
    <tab>BulkSchedule__c</tab>
    <tab>Citizen_Portal</tab>
    <tab>Open311_API_Log__c</tab>
</CustomApplication>
