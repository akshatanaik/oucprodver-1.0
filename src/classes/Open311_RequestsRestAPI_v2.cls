@RestResource(urlMapping='/v2/requests/*')
global class Open311_RequestsRestAPI_v2{

	static string jsonString;

    /*
        @Purpose:-
        Create service requests
        
        Format sent:- Content-Type: application/x-www-form-urlencoded
        HTTP Method POST
        Formats returned    XML/JSON 
        API Key: YES
        
        Sample Call:- https://[API endpoint]/requests.[format]
        
   */    
   @HttpPost
   global static void createRequest() {    
    
         RestRequest req = RestContext.request;
         
         if(RestContext.response==null) RestContext.response = new  RestResponse();
         RestResponse response = RestContext.response; 
         String format = req.requestURI.split('\\.').size()>1?req.requestURI.split('\\.')[1]:'';
         
         String responseStr; 
         
         /*
            API KEY SUPPORT             
         */
         String apiKey = req.params.get('api_key'); 
         
         APIValidations.APIKeyResponse apiKeyRes= Open311APIHelper.checkAPIKey(apiKey,req,response,format); 
         if(apiKeyRes==null) {
         	Open311APIHelper.logRequest(req,'Error','API Key not provided: Unable to create a service request');
         	return;
         }
         
          
         /*
          check to see whether jurisdiction_id is passed
         */
         String jId = req.params.get('jurisdiction_id'); 
         /*
           validate that the Juridictoion Id is correct 
         */
         if(jId!=null && !APIValidations.isValidJuridiction(jId)){
            //invalid juridiction provided:-
            response.statuscode =404; 
            responseStr =  req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                    CustomException.sendJSONApiErrorResponse('Invalid jurisdiction_id provided'):
                    CustomException.sendXMLApiErrorResponse('Invalid jurisdiction_id provided'); 
            RestContext.response.responseBody =  Blob.valueOf(responseStr);
            
            Open311APIHelper.logRequest(req,'Error','Invalid jurisdiction_id provided : '+jId);
               
            return;     
         }  
         
         //we need to get the address validation here before doing any transaction
         if(req.params.containsKey('address_id') || req.params.containsKey('address_string'))
         		jsonstring = AddressValidation.verifyAddress(req.params.get('address_id')!=null ? req.params.get('address_id'):req.params.get('address_string'));
          
         
         //create a save point
         Savepoint sp = Database.setSavepoint();
         Case c = new Case();     
         //Create a service request:-
         try{
         	 
	       	 c.Origin = '311-App';  
	         populateCaseData(c,req);   
	         
	          /* Update Private Class2 Extended Attributes if appropriate  */
	         if(apiKeyRes.isPrivate){
	           populatePrivateCaseData(c,req);
	         }
	         
	               
	         insert c;
	         
	         /*Update :- Based on the api an expanded set of parameters will be expected */ 
	         if(apiKeyRes.isPrivate  && !String.isBlank(req.params.get('tasks'))){
	            createActivities(c.Id,req);
	         }
	         
	         createflexNotes(c.Id,req);
	         
	         c = ServiceRequestsHelper.getRequest(c.Id);  
	         
	         //send the response
	         responseStr =  req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json' ?
	         Open311_ServiceRequestsAPIResponse.sendJSONResponse(c,apiKeyRes.isPrivate):
	         Open311_ServiceRequestsAPIResponse.sendXMLResponse(c,apiKeyRes.isPrivate); 
	         
	         Open311APIHelper.logRequest(req,'Success','Service request successfully created with request id: '+c.id);
	         
         }catch(Exception e){
         	 Database.rollback(sp);
         	 system.debug(e.getMessage());
         	 //delete c;
         	 responseStr= req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                          CustomException.sendJSONApiErrorResponse('Error encountered while submitting the request:'+e.getMessage()):
                          CustomException.sendXMLApiErrorResponse('Error encountered while submitting the request:'+e.getMessage());  
                          
              Open311APIHelper.logRequest(req,'Error','Error encountered while submitting the request:'+e.getMessage());              
         }
         
         RestContext.response.responseBody =  Blob.valueOf(responseStr);   
          
   }
   
   /*
        @Purpose:-
        Updates service requests
        
        Format sent:- Content-Type: application/x-www-form-urlencoded
        HTTP Method PUT
        Formats returned    XML/JSON
        API Key: YES
        
        Sample Call:- https://[API endpoint]/requests.[format]      
   */   
   @HttpPut
   global static void doUpdate() {    
    
         RestRequest req = RestContext.request; 
         if(RestContext.response==null) RestContext.response = new  RestResponse();
        
         RestResponse response = RestContext.response; 
         
         //get the response format
         String format = req.requestURI.split('\\.').size()>1?req.requestURI.split('\\.')[1]:'';
         
         String responseStr;
         
         /*
            API KEY SUPPORT             
         */
         String apiKey = req.params.get('api_key'); 
         
         APIValidations.APIKeyResponse apiKeyRes= Open311APIHelper.checkAPIKey(apiKey,req,response,format); 
         if(apiKeyRes==null || apiKeyRes.isPublic){         	 
        	 Open311APIHelper.logRequest(req,'Error','Service request could not be updated: '+
        	 										  apiKeyRes==null ? 'API Key not provided':'Update not available to public');
        	 return;
         }
         
         
         /*
          check to see whether case id is passed
         */
         String requestId = req.params.get('service_request_id');
         if(requestId==null){
            response.statuscode =404; 
            responseStr= req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                   CustomException.sendJSONApiErrorResponse('Service_request_id not provided'):
                   CustomException.sendXMLApiErrorResponse('Service_request_id not provided');
            RestContext.response.responseBody =  Blob.valueOf(responseStr);   
            Open311APIHelper.logRequest(req,'Error','Service_request_id not provided for update');
            return; 
         } 
         
         /*
          check to see whether jurisdiction_id is passed
         */
         String jId = req.params.get('jurisdiction_id'); 
         /*
           validate that the Juridictoion Id is correct 
         */
         if(jId!=null && !APIValidations.isValidJuridiction(jId)){
            //invalid juridiction provided:-
            response.statuscode =404; 
            responseStr= req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                   CustomException.sendJSONApiErrorResponse('Invalid jurisdiction_id provided'):
                   CustomException.sendXMLApiErrorResponse('Invalid jurisdiction_id provided');
            RestContext.response.responseBody =  Blob.valueOf(responseStr);  
            Open311APIHelper.logRequest(req,'Error','Invalid jurisdiction_id provided:'+jId); 
            return;        
         }  
          
         if(req.params.containsKey('address_id') || req.params.containsKey('address_string'))
         		jsonstring = AddressValidation.verifyAddress(req.params.get('address_id')!=null ? req.params.get('address_id'):req.params.get('address_string'));
         
         
         Savepoint sp = Database.setSavepoint();
         
         //Create a service request:-
         try{
             Case c = ServiceRequestsHelper.getRequest(requestId);  
             // Update Base Open311 attributes
             populateCaseData(c,req);  
             
            // Update Private Class2 Extended Attributes
             populatePrivateCaseData(c,req);
             
             update c;
             
             /*Update :- Based on the api an expanded set of parameters will be expected */ 
             if(apiKeyRes.isPrivate && !String.isBlank(req.params.get('tasks'))){
                createActivities(c.Id,req);
             }
             
             c = ServiceRequestsHelper.getRequest(requestId);
           
             responseStr = req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json' ?
                           Open311_ServiceRequestsAPIResponse.sendJSONResponse(c,apiKeyRes.isPrivate):
                           Open311_ServiceRequestsAPIResponse.sendXMLResponse(c,apiKeyRes.isPrivate); 
             
             Open311APIHelper.logRequest(req,'Success','The given service request has been updated:'+requestId); 
                 
         }catch(CustomException e){             
                Database.rollback(sp);
                String message;
                response.statuscode =400; 
                message = e.getMessage();
                if(APIConstants.NOT_FOUND==e.getMessage()){
                    response.statuscode =404;   
                    message = 'Invalid service_request_id provided';   
                      
                }               
                responseStr= req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                   		     CustomException.sendJSONApiErrorResponse('Error while updating the record with request id:'+requestId+' Error message:'+message):
                    		 CustomException.sendXMLApiErrorResponse('Error while updating the record with request id:'+requestId+' Error message:'+message);               
         
         		Open311APIHelper.logRequest(req,'Error','Error while updating the record with request id:'+requestId+'| Error message:'+message); 
         } 
         
         //logging to be done here
         
         RestContext.response.responseBody =  Blob.valueOf(responseStr); 
           
   }
   
    /*
        GET Service Request(s)
        @Purpose :-
         Query the current status of multiple requests
        
        or 
        When request id is passed:-Query the current status of an individual request
      
        Formats XML /JSON
        HTTP Method - GET
        Requires API  - NO
        
        @Params:-
        jurisdiction_id :- This is only required if the endpoint serves multiple jurisdictions        
        service_code :- The service_code is specified in the main URL path rather than an added query string parameter.
        
        Sample Call:-
        https://[API endpoint]/requests.[format]
        https://[API endpoint]/requests/[requests_id].[format]
        
        1. GET /requests - Returns a collection of sObjects 
             
           
         2. GET /requests/service_request_id - Returns a service  request
         
    */
   @HttpGet
   global static void getRequests() {
         RestRequest req = RestContext.request; 
         
         if(RestContext.response==null) RestContext.response = new  RestResponse();
         
         RestResponse response = RestContext.response; 
         
         String format = req.requestURI.split('\\.').size()>1?req.requestURI.split('\\.')[1]:'';
         
         String responseStr;
         
         
         /*
            API KEY SUPPORT             
         */
         String apiKey = req.params.get('api_key'); 
         
         APIValidations.APIKeyResponse apiKeyRes= Open311APIHelper.checkAPIKey(apiKey,req,response,format); 
         if(apiKeyRes==null ){
            apiKeyRes = new APIValidations.APIKeyResponse();
            apiKeyRes.isPrivate=false;
         }
          
         /*
          check to see whether jurisdiction_id is passed
         */
         String jId = req.params.get('jurisdiction_id');
         
         /*
           validate that the Juridictoion Id is correct 
         */
         if(jId!=null && !APIValidations.isValidJuridiction(jId)){
            //invalid juridiction provided:-
            response.statuscode =404; 
            responseStr= req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                   CustomException.sendJSONApiErrorResponse('Invalid jurisdiction_id provided'):
                   CustomException.sendXMLApiErrorResponse('Invalid jurisdiction_id provided');
            RestContext.response.responseBody =  Blob.valueOf(responseStr);   
             Open311APIHelper.logRequest(req,'Error','Invalid jurisdiction_id provided:'+jId); 
            return;   
         }  
         
         try{ 
             // see if a service code was  part of the URI
             String requestId= req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
             requestId = requestId.split('\\.').size()>1?requestId.split('\\.')[0]:requestId;
             if (requestId!= '' && !requestId.startsWith('requests')) {
                  //we have the service code here  : this is always done only if metadat =true
                  responseStr =req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                               Open311_ServiceRequestsAPIResponse.sendJSONResponse(ServiceRequestHistoryHelper.getRequest(requestId),apiKeyRes.isPrivate):
                               Open311_ServiceRequestsAPIResponse.sendXMLResponse(ServiceRequestHistoryHelper.getRequest(requestId),apiKeyRes.isPrivate);              
             } else {                
                  
                 /*
                   @updte support for additional params:
                  */
                  List<String> filterConditions= new List<String>();
                  
                  // Jurisdiction ID parameter in Open311 URL applies to entire OUC 311 system scope
                  // DOES NOT APPLY to custom object jurisdiction
                  //if(jId!=null) filterConditions.add(' SRType_Jurisdiction__r.Jurisdiction__c=\''+jId+'\'');

                   //service_request_id - when provided overrides all other parameters
                  if(req.params.get('service_request_id')!= null){
                    List<String> ids = req.params.get('service_request_id').split(',');
                    filterConditions.add(' CaseNumber__c IN (\''+String.join(ids,'\',\'')+'\')');
                  }
                  else {
	                  // Definition below is NOT final and pending clarification with integration partners
	                  if(req.params.get('servicing_agency')!= null)filterConditions.add(' Servicing_Agency__c=\''+req.params.get('servicing_agency')+'\'');
	                  
	                  
	                  //service_code
	                  if(req.params.get('service_code')!= null){
	                    List<String> ids = req.params.get('service_code').split(',');
	                    //filterConditions.add(' Service_Request_Type_Code__c IN (\''+String.join(ids,'\',\'')+'\')');
	                    // above field valid for Case, not for Service Request History
	                    filterConditions.add(' Service_Type_Code__c IN (\''+String.join(ids,'\',\'')+'\')');
	                  }
	                  
	                  //start_date
	                  if(req.params.get('start_date')!= null){
	                    	//filterConditions.add(' CreatedDate >='+Open311APIHelper.convertTimestamp(req.params.get('start_date')));
	                    	// Open311APIHelper.convertTimestamp returns a string with spaces : submitted w3 time formats are perfectly understood by SOQL
	                  		filterConditions.add(' DateTimeOpened__c >='+req.params.get('start_date'));
	                  }
	                  
	                  //end_date
	                  if(req.params.get('end_date')!= null){
	                    	// see comments on start_date
	                    	filterConditions.add(' DateTimeOpened__c <='+req.params.get('end_date'));
	                  }
	                  //String updateDate = req.params.get('update_date')!=null ? :' CreatedDate >= LAST_90_DAYS ';
	                  if(req.params.get('update_date')!=null)
	                  		// see comments on start_date
	                  		filterConditions.add(' LastUpdateDateTime__c >='+req.params.get('update_date'));
	                  
	                  
	                  //status - public status:  Only Open & Closed are supported - determined based By Case.IsClosed
	                  if(req.params.get('status')!= null){
	                     List<String> ids = req.params.get('status').split(',');
	                     filterConditions.add(' Status__c IN (\''+String.join(ids,'\',\'')+'\')');
	                     //if(req.params.get('status')=='open')
	                     //	filterConditions.add(' IsClosed=false');
	                     //else
	                     //	filterConditions.add(' IsClosed=true');
	                  }
	                 
	                  if(filterConditions.size()==0){
	                  	filterConditions.add(' CreatedDate >= LAST_90_DAYS ');
	                  }
                  }
                  responseStr =req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                               Open311_ServiceRequestsAPIResponse.sendJSONResponse(ServiceRequestHistoryHelper.listAllRequests(String.join(filterConditions,' AND ')),apiKeyRes.isPrivate):
                               Open311_ServiceRequestsAPIResponse.sendXMLResponse(ServiceRequestHistoryHelper.listAllRequests(String.join(filterConditions,' AND ')),apiKeyRes.isPrivate);      
             } 
             
              Open311APIHelper.logRequest(req,'Success','Successful return of the requested data. '); 
              
         }catch(CustomException e){             
                String message;
                response.statuscode =400; 
                message = e.getMessage();
                if(APIConstants.NOT_FOUND==e.getMessage()){
                    response.statuscode =404;   
                    message = 'Invalid service_request_id provided';   
                      
                }               
                responseStr= req.headers.get('Content-Type')==APIConstants.CONTENT_TYPE_JSON || format=='json'?
                   		     CustomException.sendJSONApiErrorResponse(message):
                    		 CustomException.sendXMLApiErrorResponse(message);          
                    		 
                Open311APIHelper.logRequest(req,'Error',message);              
         }  
         RestContext.response.responseBody =  Blob.valueOf(responseStr);
   }
   
   /*
    HELPER METHODS
   */
   
   private static void populateCaseData(Case c,RestRequest req ){
        
        //if(!String.isBlank(req.params.get('description')))c.subject = req.params.get('description'); 
        if(req.params.containsKey('description'))c.description = req.params.get('description'); 
        if(req.params.containsKey('device'))c.device__c = req.params.get('device'); 
        
         
         
        if(req.params.containsKey('service_code')){
            ServiceRequestType__c srt = ServicesHelper.getService(req.params.get('service_code'));
            c.srtype__c =srt.id;
            c.Subject = srt.service_name__c;
            c.AgencyCode__c = srt.Agency__c;
            c.SLA__c = srt.SLA__c;
        }
         
         /*
          @populate address data
         */
         //if(!String.isBlank(req.params.get('address_string')))c.Address__c = req.params.get('address_string');
         if(req.params.containsKey('address_id') || req.params.containsKey('address_string')){
         	
         	c.AddressId__c = req.params.get('address_id');
         	c.Address__c = req.params.get('address_string');
         	
         	JSONParser parser = JSON.createParser(jsonString);
        	AddressValidationResponseWrapper.AddressValidationWrapper m = (AddressValidationResponseWrapper.AddressValidationWrapper)parser.readValueAs(AddressValidationResponseWrapper.AddressValidationWrapper.class);
         	
         	//if the address is valid dump the field here
         	AddressValidationResponseWrapper.returnDatasetWrapper check_data_in_returnDataset=m.returnDataset;
         	List<AddressValidationResponseWrapper.TableWrapper> check_data_in_table= new List<AddressValidationResponseWrapper.TableWrapper>();
         	if(check_data_in_returnDataset !=null){
         		check_data_in_table=m.returnDataset.Table1;
         		
         		c.GeoLocation__Latitude__s= decimal.valueof(check_data_in_table[0].LATITUDE);
         		c.GeoLocation__Longitude__s=decimal.valueof(check_data_in_table[0].LONGITUDE);
         		c.IntersectionId__c=check_data_in_table[0].INTERSECTIONID;
         		c.AliasName__c=check_data_in_table[0].ALIASNAME;
         		c.address__c=check_data_in_table[0].FULLADDRESS+' Washington, DC '+check_data_in_table[0].ZIPCODE;
         		c.addressid__c=check_data_in_table[0].ADDRESS_ID!=null?String.valueof(Integer.valueof(check_data_in_table[0].ADDRESS_ID)):'';
         		c.quadrant__c=check_data_in_table[0].QUADRANT;
         		c.anc__c=check_data_in_table[0].ANC;
         		c.poldistrict__c=check_data_in_table[0].POLDIST;
         		c.psa__c=check_data_in_table[0].PSA;
         		c.ward__c=check_data_in_table[0].WARD;
         		c.smd__c=check_data_in_table[0].SMD;
         		c.cluster__c=check_data_in_table[0].CLUSTER;
         		c.xcoord__c=check_data_in_table[0].YCOORD;
         		c.ycoord__c=check_data_in_table[0].XCOORD;
         	}
         
         }
         
         // ER:  If we get valid address/lan/lon from MAR - DO NOT overwrite with submitted lat/lon below
         /*
         if(req.params.get('lat')!=null && req.params.get('long')!=null){
            c.GeoLocation__Latitude__s = decimal.valueof(req.params.get('lat'));
            c.GeoLocation__Longitude__s = decimal.valueof(req.params.get('long'));
         }
         */
         
         if(req.params.containsKey('media_url'))c.Media_URL__c =  req.params.get('media_url');
         
         /* 
           @ from the other details we will get the contact that will be associated with the case         
         	Branch behavior if Contact exists already (with a PUT) or if needs to be created
         */
         if(c.ContactId==null){
         	c.ContactId= ServiceRequestsHelper.createServiceRequestContact(req.params).Id;
         }
         else {
         	/* Update not supported while sharing applies to contact
         	if(!String.isBlank(req.params.get('last_name')))c.Contact.LastName = req.params.get('last_name'); 
         	if(!String.isBlank(req.params.get('first_name')))c.Contact.FirstName = req.params.get('first_name'); 
         	if(!String.isBlank(req.params.get('phone')))c.Contact.Phone = req.params.get('phone'); 
         	if(!String.isBlank(req.params.get('email')))c.Contact.Email = req.params.get('email'); 
         	*/
         }
   }
    
   private static void populatePrivateCaseData(Case c,RestRequest req ){
        /*
        Handles Private/Class2 attribute updates for POST and for PUT methods
        
        */
       if(req.params.containsKey('full_status')) c.Status=req.params.get('full_status');             
       if(req.params.containsKey('priority')) c.Priority=req.params.get('priority');
       if(req.params.containsKey('language')) c.Language__c=req.params.get('language');
       if(req.params.containsKey('external_id')) c.External_Id__c=req.params.get('external_id');
       if(req.params.containsKey('external_system_name')) c.External_System_Name__c=req.params.get('external_system_name');

         
   }
   
   private static void createflexNotes(String caseId,RestRequest req ){
        List<ServiceRequestTypeFlexNote__c> flexnotes = ServicesHelper.getServiceMetadata(req.params.get('service_code'));
                        
         Map<String,ServiceRequestTypeFlexNote__c> flexNoteMap = new Map<String,ServiceRequestTypeFlexNote__c>();
         for(ServiceRequestTypeFlexNote__c note:flexnotes ){
            //flexNoteMap.put(note.name,note);
            flexNoteMap.put(note.FlexNote_Question__r.name,note);
         }
         
      	 
         //Set<String> codes = new Set<String>();
         List<FlexNote__c>  notes= new List<FlexNote__c>();
         //Inserting flexnotes if present:-
         //  ER:  Need to handle picklists with code values including multi-select picklists
         //       Incoming submissions will use code values if available - we store answers with 
         //       code values as CODE:Human readable answer
         //       Human readable answer needs to be parsed from the flexnote question potential answers
         //       MULTI-SELECT picklist are same but you need to compile all selected answers with the same attribute[questioncode] key
         for(String key : req.params.keySet()){
             if(key.startsWith('attribute')){                   
                    String code = key.substringBetween('[',']');          
                    
                    /*
                    	validation
                    */
                    if(flexNoteMap.get(code)==null){
                    	//invalid question part of data
                    	throw new CustomException('Invalid flexnote found');
                    }
                    
                    if(flexNoteMap.get(code)!=null && flexNoteMap.get(code).FlexNote_Question__r.Required__c && String.isBlank(req.params.get(key))){
                    	throw new CustomException('Required answer for question is missing');
         				return;        		
                    }
                    
                    notes.add(new  FlexNote__c(case__c=caseId,
                                               FlexNote_Question__c=flexNoteMap.get(code).FlexNote_Question__c,
                                               Answer__c=req.params.get(key)
                                               )
                             );
                    //codes.addAll(req.params.get(key).split(','));
                    flexNoteMap.remove(code);                          
             }
         } 
         
         
         //if any flex note left out and it required throw back error 
         if(flexNoteMap.size()>0){
         	system.debug(flexNoteMap);
         	for(ServiceRequestTypeFlexNote__c extra : flexNoteMap.values()){
         		
         		if(extra.FlexNote_Question__r.Required__c){
         			throw new CustomException('Required answer for question is missing');
         			return;
         		}
         		system.debug(extra.FlexNote_Question__c);
         		notes.add(new  FlexNote__c(case__c=caseId,FlexNote_Question__c=extra.FlexNote_Question__c));
         	}
         }
         
         //before we insert notes we need to decode the available answer if any get the values from decode object
          /*List<Decode_Object__c> codeList = [Select code__c,Value__c from Decode_Object__c WHERE Code__c IN:codes];
          Map<String,String> codedAns = new Map<String,String>();
          if(codeList.size()>0){
          	for(Decode_Object__c obj:codeList)
          		codedAns.put(obj.code__c,obj.Value__c);
          }
          
          for(FlexNote__c note: notes){
          	
          	List<String> answers = new List<String>();
          	for(String code: note.answer__c.split(',')){
          		answers.add(codedAns.get(code)!=null? code+':'+codedAns.get(code) :code);
          	}
          	
          	note.codedescription__c =  String.join(answers,';');//!String.isBlank(note.answer__c) && codedAns.get(note.answer__c)!=null? note.answer__c+':'+codedAns.get(note.answer__c) :note.answer__c;
          }
          */
          if(notes.size()>0)insert notes;
   }
   
   private static void createActivities(String caseId,RestRequest req){
        List<OUC_Activity__c> activities = new List<OUC_Activity__c>();
        
        //get existing activities if any?
        Map<String,OUC_Activity__c> existing = new Map<String,OUC_Activity__c>();
           for(OUC_Activity__c t:[Select Id,Task_Code__c,Citizen_Email_On_Complete__c,Description__c,external_comments__c,			
        					   outcome__c,Display_Sort_order__c,completion_date__c,Status__c,Activity_Date__c,Responsible_Party__c  
        					   FROM OUC_Activity__c WHERE case__c=:caseId])
        	existing.put(t.Task_Code__c,t);					    
        
        
        List<ActivityWrapper> activity = (List<ActivityWrapper>) JSON.deserialize(req.params.get('tasks'),List<ActivityWrapper>.class);
        
        for(ActivityWrapper act:activity){
            OUC_Activity__c task= existing.get(act.Task_code)!=null ? existing.get(act.Task_code): new OUC_Activity__c();                                          
            
            /*
             if at any point there is no task code provided
            */ 
            if(String.isBlank(act.Task_code)){
            	throw new CustomException('Task code not provided');
            } 
                                    
            if(act.Task_code!=null) task.Task_Code__c = act.Task_code;
            if(act.Task_Short_Name!=null) task.Task_short_name__c = act.Task_Short_Name;
            if(act.Citizen_Email_On_Complete!=null) task.Citizen_Email_On_Complete__c = act.Citizen_Email_On_Complete.equalsIgnoreCase('true') ? true : false;
            if(act.Internal_Comments!=null) task.Description__c = act.Internal_Comments;
            if(act.External_Comments!=null) task.external_comments__c = act.External_Comments;
            if(act.Outcome!=null) task.outcome__c = act.Outcome;
            if(act.Order!=null) task.Display_Sort_order__c = act.Order;
            if(act.Completion_Date!=null) task.completion_date__c = act.Completion_date;
            if(act.Status!=null) task.Status__c = act.Status;
            if(act.Due_Date!=null) task.Due_Date__c = act.Due_Date;
            if(act.Responsible_Party!=null) task.Responsible_Party__c = act.Responsible_Party;
            
            task.case__c=caseId;
            activities.add(task);
        }
        
        upsert activities; 
        
   }
   
  
   
   
   /*
     JSON Wrapper Class
   */
   public class ActivityWrapper{
        public string Task_code {get;set;}
        public string Task_Short_Name{get;set;}
        public string Citizen_Email_On_Complete {get;set;}
        public string Internal_Comments {get;set;}
        public string External_Comments {get;set;}
        public string Outcome {get;set;}
        public string Order {get;set;}
        public DateTime Completion_date {get;set;}
        public string Status {get;set;}
        public DateTime Due_Date {get;set;}
        public string Responsible_Party {get;set;}
   }
   
}