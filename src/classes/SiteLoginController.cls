/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {    

    public String Email { get; set; }
    global String username{get; set;}
    global String password {get; set;}   

    global SiteLoginController () {
      
    }
    
    global PageReference login() { 

        return Site.login(username,password ,null);
       
    }    
    
}