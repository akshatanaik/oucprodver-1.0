trigger OUCActivitybeforeinsert on OUC_Activity__c (before insert) 
{
/*
* This trigger verifies Task Code Text, if its not empty it fetches the related data from Activity type object
* Created by Swetha Sathya
* Created date: 06-10-2014 
*/
/***** 2014-12-16 Trigger functionality superseded by formulas to combine UI Task Code versus Open311 Task code & related values
/////      ALSO - DEFAULT Activity Type deleted so throwing error

    list<String> taskCode = new list<String>();
    Set<Id> taskcodeSet = new Set<Id>();
    
    for(OUC_Activity__c o:trigger.new){
        if((o.Task_Code__c != null && o.Task_Code__c!='') && (o.Activity_Type__c==null)){
            taskCode.add(o.Task_Code__c.trim());
        }
        if((o.Task_Code__c == null || o.Task_Code__c=='') && (o.Activity_Type__c!=null)){
            taskcodeSet.add(o.Activity_Type__c);
        }
    }
    
    Map<String,Id> activityMap = new Map<String,Id>();
    Map<Id,String> taskcodeMap = new Map<Id,String>();
    
    for(Activity_Type__c  obj:[SELECT Id,Name FROM Activity_Type__c WHERE Name='DEFAULT'])
        activityMap.put(obj.Name,obj.Id);
        
    for(Activity_Type__c  obj:[SELECT Id,Name FROM Activity_Type__c WHERE Name IN:taskCode])
        activityMap.put(obj.Name,obj.Id);
        
     for(Activity_Type__c  obj:[SELECT Id,Name FROM Activity_Type__c WHERE Id IN:taskcodeSet])
        taskcodeMap .put(obj.Id,obj.Name);
        
    system.debug('----activityMap----------'+activityMap);
    system.debug('----taskcodeMap ----------'+taskcodeMap );
    for(OUC_Activity__c o:trigger.new){
        if((o.Task_Code__c != null && o.Task_Code__c!='') && (o.Activity_Type__c==null)){
            if(activityMap!=null && activityMap.size()>0){
                if(activityMap.KeySet().contains(o.Task_Code__c.trim()))
                    o.Activity_Type__c = activityMap.get(o.Task_Code__c.trim());
                else
                     o.Activity_Type__c = activityMap.get('DEFAULT');
            }
        }
        if((o.Task_Code__c == null || o.Task_Code__c!='') && (o.Activity_Type__c!=null)){
            if(taskcodeMap !=null && taskcodeMap.size()>0){
                if(taskcodeMap.KeySet().contains(o.Activity_Type__c))
                    o.Task_Code__c= taskcodeMap.get(o.Activity_Type__c);
                else
                     o.Task_Code__c= taskcodeMap.get('DEFAULT');
            }
        }
    }
*******/    
    
}