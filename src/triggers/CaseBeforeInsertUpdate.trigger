trigger CaseBeforeInsertUpdate on Case (before insert, before update) {
	/*
	* This trigger sets Expected Bussiness Resolution date based on createdDate ,sla and Standard Bussines Hours values as inputs
	* Created by Devanand
	* Created date: 04-12-2014 
	*/
	
	try{
		list<Id>serviceTypeId=new list<Id>();
		for(case c:trigger.new){
			serviceTypeId.add(c.SRType__c);
		}
		
		list<ServiceRequestType__c>serviceTypeList=[Select s.SLA__c, s.Name, s.Id From ServiceRequestType__c s where id In:serviceTypeId];
		
		// invoking method update the field ExpectedBusinessResolutionDate__c in case 
		ServiceRequestsHelper.setExceptedBussinessDateTime(trigger.new,serviceTypeList);
	}
	catch(exception e){
		system.debug('unable to complete set bussinessDate--'+e);
	}
}