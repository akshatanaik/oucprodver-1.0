<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Last_Update_placeholder</fullName>
        <description>placeholder to update last update until workflow arrives</description>
        <field>Last_Update_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Last Update placeholder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Last Update placeholder</fullName>
        <actions>
            <name>Last_Update_placeholder</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>pending workflow</description>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
